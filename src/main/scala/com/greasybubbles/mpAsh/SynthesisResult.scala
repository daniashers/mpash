package com.greasybubbles.mpAsh

class SynthesisResult(
    val pcmLeft: Array[Int],
    val pcmRight: Array[Int],
    val rollingSynthData: RollingSynthData,
    val benchmarkData: BenchmarkData,
    val spectrum: (Array[Double], Array[Double]) )