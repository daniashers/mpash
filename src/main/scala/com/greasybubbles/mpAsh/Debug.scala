package com.greasybubbles.mpAsh

object Debug {
	def printArray(a: Array[Double]): Unit = {
	  print("Size %d = (".format(a.size))
	  for (i <- 0 to a.size-1) print("%f ".format(a(i)))
	  println(")")
	}
	
	def printArray(a: Array[Int]): Unit = {
	  print("Size %d = (".format(a.size))
	  for (i <- 0 to a.size-1) print("%d ".format(a(i)))
	  println(")")
	}
	
	def printArrayShort(a: Array[Short]): Unit = {
	  print("Size %d = (".format(a.size))
	  for (i <- 0 to a.size-1) print("%d ".format(a(i)))
	  println(")")
	}
	
	def log(s: String): Unit = println(s)
}