package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

object Mp3Util {

	//TODO to optimise....
	def crc16(s: BitStream): Int = {

	  @annotation.tailrec
	  def crc16_loop(sr: BitStream): Int = {
		val poly: Int = 0x8005 
		if (sr.len <= 16) new ReversedStream(sr).getUint(0,16)
	    else {
	      if (sr.head == true) {
	        val intermediateResult = BitStream.fromInt(sr.getUint(1,16) ^ poly, 16) + sr.sub(17, sr.len - 17)
	    	crc16_loop(intermediateResult)
	      } else {
	        crc16_loop(new LogicalBitStream(sr, 1, sr.len-1))
	      }
	    }
	  }	  
	  

	  val input = s + BitStream.literalStream("0000000000000000")
	  crc16_loop(input)
	}
	

	def fastSum(first: Int, last: Int, f: Int => Double): Double = {
	  @annotation.tailrec
	  def go(frst: Int, lst: Int, accu: Double): Double = {
	    if (frst == lst) accu + f(frst)
	    else go(frst+1, lst, accu + f(frst))
	  }
	  go(first,last, 0.0)
	}
	
	def sumInt(first: Int, last: Int, f: Int => Int): Int = {
	  @annotation.tailrec
	  def go(frst: Int, lst: Int, accu: Int): Int = {
	    if (frst == lst) accu + f(frst)
	    else go(frst+1, lst, accu + f(frst))
	  }
	  go(first,last,0)
	}
}

//a stripped down version of Frame that only calculates its own length and checks for a correct header
class FrameSync(base: BitStream, start: Int, length: Int = 57) extends LogicalBitStream(base,start,length) {
  val syncword = 		this.sub(0, 12)
  val algorithmId = 	this.getUint(MP3_ALGORITHM_ID)
  val layer = 			this.getUint(MP3_LAYER)
  val bitrateIndex = 	this.getUint(MP3_BITRATE_INDEX)
  val sampleFreqIndex = this.getUint(MP3_SAMPLE_FREQ_INDEX)
  val paddingFlag = 	this.getUint(MP3_PADDING_FLAG)
  val protectionBit =   this.getUint(MP3_CRC_FLAG)

  val bitrate: Option[Int] = Mp3Constants.bitrate(bitrateIndex)
  val sampleRate: Option[Int] = Mp3Constants.sampleRate(sampleFreqIndex)
  // returns the length of the frame in bits
  lazy val frameLength: Int = 8 * ((144 * bitrate.get / sampleRate.get) + (if (paddingFlag==1) 1 else 0))

  def mainDataBegin = if (protectionBit == 1) this.getUint(32,9) * 8 //we reckon all in bits not bytes
  						else this.getUint(48,9) * 8
  
  val isValid: Boolean = syncword.equals(BitStream.literalStream("111111111111")) && 
  							bitrate != None && sampleRate != None
  val isSupported: Boolean = (layer == LAYER_III && algorithmId == MPEG_1_0)
}