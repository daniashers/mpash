package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._
import com.greasybubbles.mpAsh.BlockSideInfo._

class BlockSideInfo(parent: BitStream, start: Int, length: Int) extends LogicalBitStream(parent: BitStream, start: Int, length: Int) {
    
	val part23Length = 		getUint(MP3_GRANULE_PART2_3_LENGTH) //length of scalefactors + length of Huffman
	val bigValues = 		getUint(MP3_GRANULE_BIG_VALUES)
	val globalGain = 		getUint(MP3_GRANULE_GLOBAL_GAIN)
	val scalefacCompress = 	getUint(MP3_GRANULE_SCALEFAC_COMPRESS)
	val blockSplitFlag = 	getUint(MP3_GRANULE_BLOCKSPLIT_FLAG)

	//following only appear for BlockSplit flag set
	val bsBlockType = 		getUint(MP3_GRANULE_BS_BLOCK_TYPE)
	val bsSwitchPoint = 	getUint(MP3_GRANULE_BS_SWITCH_POINT)
	val bsSubGainWnd0 = 	getUint(MP3_GRANULE_BS_SUB_GAIN_WND0)
	val bsSubGainWnd1 = 	getUint(MP3_GRANULE_BS_SUB_GAIN_WND1)
	val bsSubGainWnd2 = 	getUint(MP3_GRANULE_BS_SUB_GAIN_WND2)

	//following only appear for BlockSplit flag clear
	val nbsRegionAddress1 = 	getUint(MP3_GRANULE_NBS_REGION_ADDRESS1)
	val nbsRegionAddress2 = 	getUint(MP3_GRANULE_NBS_REGION_ADDRESS2)
	
	//following appear always.
	val preFlag = 			getUint(MP3_GRANULE_PREFLAG)
	val scalefacScale = 	getUint(MP3_GRANULE_SCALEFAC_SCALE)
	val count1TableSelect =	getUint(MP3_GRANULE_COUNT1TABLE_SELECT)
	
	//
	//the following values are not retrieved straight from the bit stream but calculated from it.
	//
	val subblockGain = Seq(bsSubGainWnd0, bsSubGainWnd1, bsSubGainWnd2)
	
	val huffmanTable0 = blockSplitFlag match {
	  case 0 => getUint(MP3_GRANULE_NBS_TABLE_SELECT_REG0)
	  case 1 => getUint(MP3_GRANULE_BS_TABLE_SELECT_REG0)
	}
	
	val huffmanTable1 = blockSplitFlag match {
	  case 0 => getUint(MP3_GRANULE_NBS_TABLE_SELECT_REG1)
	  case 1 => getUint(MP3_GRANULE_BS_TABLE_SELECT_REG1)
	}
	
	val huffmanTable2 = blockSplitFlag match {
	  case 0 => getUint(MP3_GRANULE_NBS_TABLE_SELECT_REG2)
	  case 1 => 0
	}
	
	val regionAddress1 = blockSplitFlag match {
	  case 0 => nbsRegionAddress1 + 1
	  case 1 => {
	    if (blockType == SHORT_WINDOWS) 8 else 8
	  }
	}
	val regionAddress2 = blockSplitFlag match {
	  case 0 => nbsRegionAddress2 + 1
	  case 1 => 0
	}
	
	def region01Boundary: Int = ScalefactorBand.band(regionAddress1, 44100, false).get.bottom min (bigValues * 2)
	def region12Boundary: Int = {
	  if (blockType == NORMAL) ScalefactorBand.band(regionAddress1 + regionAddress2, 44100, false).get.bottom min (bigValues * 2)
	  else bigValues * 2
	}
	def region23Boundary: Int = bigValues * 2
		
	def region0Count: Int = region01Boundary
	def region1Count: Int = region12Boundary - region01Boundary
	def region2Count: Int = region23Boundary - region12Boundary

	val slen1 = ScalefactorBand.slen1(scalefacCompress)
	val slen2 = ScalefactorBand.slen2(scalefacCompress)
		
	def blockType: Int = blockSplitFlag match {
		case 0 => NORMAL
		case 1 => bsBlockType match {
		  			case 0 => INVALID
		  			case 1 => START_BLOCK
		  			case 2 => SHORT_WINDOWS
		  			case 3 => END_BLOCK
		}
	}
}

object BlockSideInfo {
    //the following offsets are given with respect to the beginning of a granule
	//and not to the beginning of the frame or of the fixed audio data.
	val MP3_GRANULE_PART2_3_LENGTH = 		new OffsetAndLength(0, 12)
	val MP3_GRANULE_BIG_VALUES =			new OffsetAndLength(12, 9)
	val MP3_GRANULE_GLOBAL_GAIN =			new OffsetAndLength(21, 8)
	val MP3_GRANULE_SCALEFAC_COMPRESS =		new OffsetAndLength(29, 4)
	val MP3_GRANULE_BLOCKSPLIT_FLAG = 		new OffsetAndLength(33, 1)
	
	val MP3_GRANULE_BS_BLOCK_TYPE =			new OffsetAndLength(34, 2)
	val MP3_GRANULE_BS_SWITCH_POINT = 		new OffsetAndLength(36, 1)
	val MP3_GRANULE_BS_TABLE_SELECT_REG0 = 	new OffsetAndLength(37, 5)
	val MP3_GRANULE_BS_TABLE_SELECT_REG1 = 	new OffsetAndLength(42, 5)
	val MP3_GRANULE_BS_SUB_GAIN_WND0 = 		new OffsetAndLength(47, 3)
	val MP3_GRANULE_BS_SUB_GAIN_WND1 = 		new OffsetAndLength(50, 3)
	val MP3_GRANULE_BS_SUB_GAIN_WND2 = 		new OffsetAndLength(53, 3)
	
	val MP3_GRANULE_NBS_TABLE_SELECT_REG0 = new OffsetAndLength(34, 5)
	val MP3_GRANULE_NBS_TABLE_SELECT_REG1 = new OffsetAndLength(39, 5)
	val MP3_GRANULE_NBS_TABLE_SELECT_REG2 = new OffsetAndLength(44, 5)
	val MP3_GRANULE_NBS_REGION_ADDRESS1 = 	new OffsetAndLength(49, 4)
	val MP3_GRANULE_NBS_REGION_ADDRESS2 = 	new OffsetAndLength(53, 3)
	
	val MP3_GRANULE_PREFLAG = 				new OffsetAndLength(56, 1)
	val MP3_GRANULE_SCALEFAC_SCALE = 		new OffsetAndLength(57, 1)
	val MP3_GRANULE_COUNT1TABLE_SELECT = 	new OffsetAndLength(58, 1)

}