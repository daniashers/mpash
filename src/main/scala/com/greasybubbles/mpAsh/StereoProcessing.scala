package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

class Channel
case class Left() extends Channel
case class Right() extends Channel

object StereoProcessing {
  
  //val LEFT = false
  //val RIGHT = true
	
  def processSubband(ch0: IBlock, ch1: IBlock, stereoMode: StereoMode, leftRight: Boolean, i: Int): Subband = {

    val targetChannel = if (leftRight == false) ch0 else ch1
	  	
    val lowestCoefficientOfThisSubband = 18*i
    val highestCoefficientOfThisSubband = 18*(i+1) - 1

    val highestNonzeroCh1FreqLine = {
      var c: Int = 575
      while (c > 0 && ch1.freqLine(c) == 0.0) c = c-1 //double compare to zero! dangerous! what if very small?
      c
    }	  

    def intensityPan(j: Int): Double = {
      val coefficientNumber = 18*j
      val originalValue = targetChannel.freqLine(coefficientNumber)
      if (coefficientNumber <= highestNonzeroCh1FreqLine) originalValue
      else {
        val scalefactorBandToWhichItBelongs: Int = ScalefactorBand.getBandFor(coefficientNumber, false).get
        val scalefactorValue = ch1.scalefactors(scalefactorBandToWhichItBelongs)
        if (scalefactorValue < 0 || scalefactorValue >=7 ) originalValue
        else originalValue * (if (leftRight) panTable(6-scalefactorValue) else panTable(scalefactorValue))
      }
    }

    //TODO: in the below code, a subband is either decoded TOTALLY as intensity stereo, or
    //totally as normal stereo. It doesn't consider the case where the boundary between normal and intensity stereo
    //falls in the middle of a sideband. It's good enough but it's not 100% spec.
    if (highestNonzeroCh1FreqLine > lowestCoefficientOfThisSubband) { //is at least part of this subband in nonzero territory?
      if (stereoMode.isMiddleSide) processMiddleSide(ch0, ch1, leftRight, i) //apply mid-side stereo
      else targetChannel.subband(i) //do nothing
    } else {
	  if (stereoMode.intensityOn) new Subband(Array.tabulate(18)(intensityPan(_)), ch0.subband(i).blockType)
	  else if (stereoMode.isMiddleSide) processMiddleSide(ch0, ch1, leftRight, i) //apply mid-side stereo
      else targetChannel.subband(i) //do nothing
	}
  }

  def processMiddleSide(ch0: IBlock, ch1: IBlock, leftRight: Boolean, i: Int): Subband = {
    val subband0 = ch0.subband(i)
    val subband1 = ch1.subband(i)
    leftRight match {
		case false => new Subband(Array.tabulate(18)(s => RECIPROC_SQRT2 * (subband0.samples(s) + subband1.samples(s))), ch0.blockType)
		case true  => new Subband(Array.tabulate(18)(s => RECIPROC_SQRT2 * (subband0.samples(s) - subband1.samples(s))), ch1.blockType)
    }
  }

  val RECIPROC_SQRT2 = 0.70710678118
  val panTable: Array[Double] = Array(0, 0.2113248654, 0.36602540378, 0.5, 0.63397459621, 0.78867513459, 1)
}