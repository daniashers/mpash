package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

class Frame(base: BitStream, start: Int, length: Int, bitReservoir: BitStream) extends LogicalBitStream(base,start,length) {
	val startOfNextFrame = start + length
  
    val header = this.sub(MP3_HEADER.offset, MP3_HEADER.length)
	
	//
	// Raw header values
	//
	val syncword = this.sub(MP3_SYNCWORD.offset, MP3_SYNCWORD.length)
	val algorithmId = this.getUint(MP3_ALGORITHM_ID)
	val layer = this.getUint(MP3_LAYER.offset, MP3_LAYER.length)
	val protectionBit = this.getUint(MP3_CRC_FLAG)
	val bitrateIndex = this.getUint(MP3_BITRATE_INDEX.offset, MP3_BITRATE_INDEX.length)
	val sampleFreqIndex = this.getUint(MP3_SAMPLE_FREQ_INDEX.offset, MP3_SAMPLE_FREQ_INDEX.length)
	val paddingFlag = this.getUint(MP3_PADDING_FLAG)
	val _stereoMode = this.getUint(MP3_STEREO_MODE)
	val jointStereoType = this.getUint(MP3_JOINT_STEREO_TYPE)
	val copyrightFlag = this.getUint(MP3_COPYRIGHT_FLAG)
	val emphasis = this.getUint(MP3_EMPHASIS)

	//
	// Calculated properties
	//

	val bitrate: Option[Int] = Mp3Constants.bitrate(bitrateIndex)
	val sampleRate: Option[Int] = Mp3Constants.sampleRate(sampleFreqIndex)
	// returns the length of the frame in bits
	val frameLength: Int = 8 * ((144 * bitrate.get / sampleRate.get) + (if (paddingFlag==1) 1 else 0))
	val stereoMode = StereoMode(_stereoMode, jointStereoType)
	
	//
	// Decomposition of stream
	//
	
	
	val sideInformation: BitStream = {
	  val beginning: Int = if (protectionBit == 1) MP3_SIDE_INFORMATION_NO_CRC_OFFSET
			  else MP3_SIDE_INFORMATION_WITH_CRC_OFFSET
	  val length: Int = if (_stereoMode == SINGLE_CHANNEL) MP3_SIDE_INFORMATION_SINGLE_CHANNEL_LENGTH
	  		  else MP3_SIDE_INFORMATION_MULTI_CHANNEL_LENGTH
	  this.sub(beginning,length)
	}
	
	def mainDataPortionBegin: Int = {
	  val sideInfoBeginning: Int = if (protectionBit == 1) MP3_SIDE_INFORMATION_NO_CRC_OFFSET
			  else MP3_SIDE_INFORMATION_WITH_CRC_OFFSET
	  val sideInfoLength: Int = if (_stereoMode == SINGLE_CHANNEL) MP3_SIDE_INFORMATION_SINGLE_CHANNEL_LENGTH
	  		  else MP3_SIDE_INFORMATION_MULTI_CHANNEL_LENGTH
	  sideInfoBeginning + sideInfoLength
	}
	
	def mainDataBegin = sideInformation.getUint(MP3_SIDEINFO_SC_MAIN_DATA_BEGIN) * 8 //we reckon all in bits not bytes

	val mono = (_stereoMode == SINGLE_CHANNEL)
	val intensityStereo = (jointStereoType & 0x01) != 0
	val middleSideStereo = (jointStereoType & 0x02) != 0
	
	lazy val mainData: BitStream = {
	  val mainDataLength = if (mono) gr0ch0si.part23Length + gr1ch0si.part23Length
			  				else gr0ch0si.part23Length + gr0ch1si.part23Length + gr1ch0si.part23Length + gr1ch1si.part23Length
	  val mainDataPart1 = bitReservoir
	  val mainDataPart2 = this.sub(mainDataPortionBegin, mainDataLength-mainDataPart1.len)
	  mainDataPart1 + mainDataPart2
	}

	lazy val body: BitStream = this.sub(mainDataPortionBegin, frameLength - mainDataPortionBegin)
	
	lazy val leftoverBits: BitStream = {
	  if (mainData.len < bitReservoir.len) bitReservoir.sub(mainData.len, bitReservoir.len - mainData.len) + body
	  else body.sub(mainData.len - bitReservoir.len, body.len - (mainData.len - bitReservoir.len)) 
	}
	
	val GSILEN = 59 // The length of each granule side info block

	//Carve the Side Info segments (granule X, channel Y side info)
	val gr0ch0si = if (mono) new BlockSideInfo(sideInformation, 18 + 0*GSILEN, GSILEN)
							  else new BlockSideInfo(sideInformation, 20 + 0*GSILEN, GSILEN)
	val gr0ch1si = if (mono) null
	                          else new BlockSideInfo(sideInformation, 20 + 1*GSILEN, GSILEN)
	val gr1ch0si = if (mono) new BlockSideInfo(sideInformation, 18 + 1*GSILEN, GSILEN)
							  else new BlockSideInfo(sideInformation, 20 + 2*GSILEN, GSILEN)
	val gr1ch1si = if (mono) null
							  else new BlockSideInfo(sideInformation, 20 + 3*GSILEN, GSILEN)
	
	
	//Carve the Main Data segments (granule X, channel Y main data)
	val gr0ch0md = new BlockMainData(mainData, 0, gr0ch0si.part23Length)
	val gr0ch1md = if (mono) null
					else new BlockMainData(mainData, 0 + gr0ch0md.len, gr0ch1si.part23Length)
	val gr1ch0md = if (mono) new BlockMainData(mainData, 0 + gr0ch0md.len, gr1ch0si.part23Length)
					else new BlockMainData(mainData, 0 + gr0ch0md.len + gr0ch1md.len, gr1ch0si.part23Length)
	val gr1ch1md = if (mono) null
					else new BlockMainData(mainData, 0 + gr0ch0md.len + gr0ch1md.len + gr1ch0md.len, gr1ch1si.part23Length) 
	
	lazy val atom0L: IBlock = {
	  if (gr0ch0si.blockType == SHORT_WINDOWS)
	    new ShortWindowBlock(gr0ch0si, gr0ch0md)
	  else
	    new LongWindowBlock(gr0ch0si, gr0ch0md, null, NO_SCFSI)
	}
	lazy val atom0R: IBlock = {
	  if (stereoMode.isMono) null
	  else if (gr0ch1si.blockType == SHORT_WINDOWS)
	    new ShortWindowBlock(gr0ch1si, gr0ch1md)
	  else
	    new LongWindowBlock(gr0ch1si, gr0ch1md, null, NO_SCFSI)
	}
	lazy val atom1L: IBlock = {
	  if (gr1ch0si.blockType == SHORT_WINDOWS)
	    new ShortWindowBlock(gr1ch0si, gr1ch0md)
	  else
	    new LongWindowBlock(gr1ch0si, gr1ch0md, atom0L, scfsiCh0)
	}
	lazy val atom1R: IBlock = {
	  if (stereoMode.isMono) null
	  else if (gr1ch1si.blockType == SHORT_WINDOWS)
	    new ShortWindowBlock(gr1ch1si, gr1ch1md)
	  else
	    new LongWindowBlock(gr1ch1si, gr1ch1md, atom0R, scfsiCh1)
	}
	
	val cStereoMode = StereoMode(_stereoMode, jointStereoType)
	
	val granule0 = new Granule(atom0L, atom0R, cStereoMode)
	val granule1 = new Granule(atom1L, atom1R, cStereoMode)
	
	//
	// 'PUBLIC' functions
	//

	val validSync: Boolean = syncword.equals(BitStream.literalStream("111111111111"))
	
	//counterintuitively if protection bit is SET -> no protection and CLEAR -> protected with crc16
	val hasCrc: Boolean = if (protectionBit == 0) true else false
	def crc16: Option[Int] = if (protectionBit == 0) Some(this.getUint(MP3_CRC16)) else None
	
	
	lazy val protectedBits: BitStream = {
	  val numberOfProtectedAudioDataBits = (if (_stereoMode == SINGLE_CHANNEL) 136 else 256)
	  this.sub(16, 16) + this.sub(48,numberOfProtectedAudioDataBits)
	}

	private def scfsiBits = if (mono) sideInformation.sub(14, 4) else sideInformation.sub(12,8)

	def scfsiCh0: Array[Boolean] = Array(scfsiBits.bit(0), scfsiBits.bit(1), scfsiBits.bit(2), scfsiBits.bit(3))
	def scfsiCh1: Array[Boolean] = if (mono) null
							else Array(scfsiBits.bit(4), scfsiBits.bit(5), scfsiBits.bit(6), scfsiBits.bit(7))

	def scfsiToString(scfsi: Array[Boolean]): String = {
		if (scfsi == null) "n/a "
		else ( (if (scfsi(0)) "1" else "0") + (if (scfsi(1)) "1" else "0") +
		       (if (scfsi(2)) "1" else "0") + (if (scfsi(3)) "1" else "0"))
	}
							
	override def toString: String = "Frame addr%8x | ".format(start/8) +
	    "%5dHz %3dkbps ".format(sampleRate.get, bitrate.get/1000) + stereoMode.toString + "\n" +
		"CRC16 " + (if (hasCrc) "%04x".format(this.crc16.get) else "  n/a   ") + 
		"  scfsi ch0 " + scfsiToString(scfsiCh0) + " ch1 " + scfsiToString(scfsiCh1) +
		" - bit reservoir %3.0f%% ".format(100.0*mainDataBegin/4096) + Mp3DisplayUtil.drawBar(1.0f*mainDataBegin/4096f, 10)
}

object Frame {
  	//returns the frame at the specified bit position. If the frame is not valid, skips ahead till it
	//finds a valid one.
	@annotation.tailrec
	def getNextFromStream(s: BitStream, pos: Int, runningReservoir: BitStream = BitStream.EMPTY): Option[Frame] = {
	  val frameSync = new FrameSync(s, pos)
	  //1.check if the frame sync signature is correct
	  //2.calculates the length
	  //3.tries to see if the header was correct by checking that
	  //after the reported length there is indeed the beginning of a new valid frame
	  if (frameSync.isValid && frameSync.isSupported && (new FrameSync(s, pos + frameSync.frameLength)).isValid) {
	    //if we pass all these checks then it looks like this frame is good: read it all and return it
		val bitReservoir = runningReservoir.sub(runningReservoir.len - frameSync.mainDataBegin, frameSync.mainDataBegin)
	    Some(new Frame(s, pos, frameSync.frameLength, bitReservoir))
	  } else {
	    //if this is not valid go ahead one byte (stay byte aligned) and try again
	    val newPos = if (pos % 8 == 0) (pos + 8) else (pos - pos%8 + 8)
	    //carve out the max possible bit reservoir as we no longer know how long it will be
	    val maxPossibleReservoirLength = 512*8 min newPos
	    val newReservoir = s.sub(newPos - maxPossibleReservoirLength, maxPossibleReservoirLength)
	    getNextFromStream(s, newPos, newReservoir)
	  }
	}
	
}