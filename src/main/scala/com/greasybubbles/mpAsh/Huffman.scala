package com.greasybubbles.mpAsh

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer

object Huffman {

  	def decodeCoefficients(g: Block): Array[Int] = {
	  val regionLength = Array(g.si.region0Count, g.si.region1Count, g.si.region2Count)
	  val regionTable = Array(g.si.huffmanTable0, g.si.huffmanTable1, g.si.huffmanTable2)
	  var coeff: ArrayBuffer[Int] = new ArrayBuffer[Int]()
	  val huffmanBits = g.huffmanCodeBits
	  var offs = 0
	  
	  // decode regions 0 to 2 ('big' values, stored in pairs)		
	  for (region <- 0 to 2) {
	    val table = HuffmanTables.pairTables(regionTable(region))
	    for (i <- 1 to regionLength(region)/2) {
	      val result = try {
	        decodePair(huffmanBits, offs, table)
	      } catch {
	        case ex: Exception => None
	      }
		  if (result != None) {
			  offs = offs + result.get.length
			  coeff.append(result.get.x)
			  coeff.append(result.get.y)
		  } else { //throw new RuntimeException("Bad huffman data in coefficient pair " + i + " of region " + region + ".")
			  coeff.append(0)
			  coeff.append(0) // CAUTION! this will mask errors... you should handle them
		  }
		}
	  }
		 
	  // decode region 3 ('count one' values, stored in quadruples)
	  var count1count = 0
	  while (offs < huffmanBits.len) {
		  val result = try {
		    decodeQuad(huffmanBits, offs, HuffmanTables.quadTables(g.si.count1TableSelect))
		  } catch {
		    case ex: Exception => None
		  }
		  if (result != None) {
		    offs = offs + result.get.length
			coeff.append(result.get.v)
			coeff.append(result.get.w)
			coeff.append(result.get.x)
			coeff.append(result.get.y)
			count1count += 4
	      } else {
	        offs = huffmanBits.len //skip to end of stream so no more is processed.
	        //throw new RuntimeException("Bad huffman data.")
	      }
	  }
	  //any coefficients not encoded in one of the regions above, is taken to be zero.
	  while (coeff.size < 576) {
	    coeff.append(0)
	  }
	  coeff.toArray[Int]
	}
  
  def decodePair(b: BitStream, offset: Int, table: Codebook[PairCode]): Option[PairWord] = {
	//read a sequence of 20 bits from the bitstream. For performance, read only as many as the
	//huffman table in use will actually look at, and pad the rest with zeroes
	val bitSequence: Int = b.getUint(offset, table.maxPatternLength) << (20-table.maxPatternLength) //make magic number 20 go away
	val p = matchPair(table, bitSequence) 
	val linbits = table.linbits
	var offs: Int = offset+p.length
	var xval = p.x
	var yval = p.y
	if (xval == 15) {
	  xval = 15 + b.getUint(offs, linbits)
	  offs = offs + linbits
	}
	if (xval != 0) {
	  if (b.bit(offs)) xval = -xval
	  offs = offs + 1
	}
	if (yval == 15) {
	  yval = 15 + b.getUint(offs, linbits)
	  offs = offs + linbits
	}
	if (yval != 0) {
	  if (b.bit(offs)) yval = -yval
	  offs = offs + 1
	}
	Some(new PairWord(xval,yval,offs-offset))
  }
  
  //note: the below method can run into problems. If the LAST quad code is only 4 bits long, and thereafter
  //the bitstream for that particular block ends, it will encounter an array out of bound exception
  //as it will attempt to read 6 bits - the last 2 of which don't exist!
	def decodeQuad(b: BitStream, offset: Int, table: Codebook[QuadCode]): Option[QuadWord] = {
      val q = matchQuad(table, b.getUint(offset,6)) //reads 6 bits as that's the maximum length of a quad code.
	  var offs: Int = offset+q.length
	  var v: Int = q.v
	  var w: Int = q.w
	  var x: Int = q.x
	  var y: Int = q.y
	  if (v != 0) {
	    if (b.bit(offs)) v = -v
	    offs = offs+1
	  }
	  if (w != 0) {
	    if (b.bit(offs)) w = -w
	    offs = offs+1
	  }
	  if (x != 0) {
	    if (b.bit(offs)) x = -x
	    offs = offs+1
	  }
	  if (y != 0) {
	    if (b.bit(offs)) y = -y
	    offs = offs+1
	  }
	  Some(new QuadWord(v,w,x,y,offs-offset))
	}
	
	/**
	 * @param ptn: a 20-bit integer read from the bitstream. (20 bit is the max length of a pair code)
	 */
	def matchPair(cbk: Codebook[PairCode], ptn: Int): PairCode = {
	    val codes: Array[PairCode] = cbk.codes
	    @annotation.tailrec
	    //this binary search does not return an exact match but the highest entry that is less or equal than.
	    def binarySearch(firstIndex: Int, lastIndex: Int): PairCode = {
	      if (lastIndex-firstIndex <= 1) {
	        if (codes(lastIndex).key <= ptn) codes(lastIndex)
	        else codes(firstIndex)
	      } else {
		      val middleIndex = (firstIndex + lastIndex)/2
		      if (codes(middleIndex).key <= ptn) binarySearch(middleIndex, lastIndex)
		      else binarySearch(firstIndex, middleIndex)
	      }
	    }
	    binarySearch(0, codes.length-1)
	}

	/**
	 * @param ptn: a 6-bit integer read from the bitstream. (6 bit is the max length of a quad code)
	 */
	def matchQuad(cbk: Codebook[QuadCode], ptn: Int): QuadCode = {
	  val codes: Array[QuadCode] = cbk.codes
	    @annotation.tailrec
	    //this binary search does not return an exact match but the highest entry that is less or equal than.
	    def binarySearch(firstIndex: Int, lastIndex: Int): QuadCode = {
	      if (lastIndex-firstIndex <= 1) {
	        if (codes(lastIndex).key <= ptn) codes(lastIndex)
	        else codes(firstIndex)
	      } else {
		      val middleIndex = (firstIndex + lastIndex)/2
		      if (codes(middleIndex).key <= ptn) binarySearch(middleIndex, lastIndex)
		      else binarySearch(firstIndex, middleIndex)
	      }
	    }
	    binarySearch(0, codes.length-1)
	}
	
}

class PairWord(val x: Int, val y: Int, val length: Int)
class QuadWord(val v: Int, val w: Int, val x: Int, val y: Int, val length: Int)
