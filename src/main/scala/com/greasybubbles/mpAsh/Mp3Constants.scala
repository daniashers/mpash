package com.greasybubbles.mpAsh

class OffsetAndLength(off: Int, len: Int) {
  def offset: Int = off
  def length: Int = len
}

object Mp3Constants {
	//all values in BITS
	val MP3_HEADER = new OffsetAndLength(0,32)

	//data contained in header
	val MP3_SYNCWORD = 					new OffsetAndLength(0, 12)
	val MP3_ALGORITHM_ID = 				new OffsetAndLength(12, 1)
	val MP3_LAYER = 					new OffsetAndLength(13, 2)
	val MP3_CRC_FLAG = 					new OffsetAndLength(15, 1)
	val MP3_BITRATE_INDEX = 			new OffsetAndLength(16, 4)
	val MP3_SAMPLE_FREQ_INDEX = 		new OffsetAndLength(20, 2)
	val MP3_PADDING_FLAG =		 		new OffsetAndLength(22, 1)
	// one bit ignored here.......................................
	val MP3_STEREO_MODE = 				new OffsetAndLength(24, 2)
	val MP3_JOINT_STEREO_TYPE = 		new OffsetAndLength(26, 2)
	val MP3_COPYRIGHT_FLAG = 			new OffsetAndLength(28, 1)
	// one bit ignored here.......................................
	val MP3_EMPHASIS = 					new OffsetAndLength(30, 2)

	//other data
	val MP3_CRC16 = 					new OffsetAndLength(32, 16)
	
	val MP3_SIDE_INFORMATION_NO_CRC_OFFSET = 32
	val MP3_SIDE_INFORMATION_WITH_CRC_OFFSET = 48

	val MP3_SIDE_INFORMATION_SINGLE_CHANNEL_LENGTH = 136
	val MP3_SIDE_INFORMATION_MULTI_CHANNEL_LENGTH = 256
	
	//the following offsets are given with respect to the beginning of 'side information'
	//and not to the beginning of the frame.
	val MP3_SIDEINFO_SC_MAIN_DATA_BEGIN = 	new OffsetAndLength(0, 9)
	val MP3_SIDEINFO_SC_PRIVATE_BITS = 		new OffsetAndLength(9, 5)
	val MP3_SIDEINFO_SC_SCFSI_BAND0 =		new OffsetAndLength(14, 1)
	val MP3_SIDEINFO_SC_SCFSI_BAND1 = 		new OffsetAndLength(15, 1)
	val MP3_SIDEINFO_SC_SCFSI_BAND2 =		new OffsetAndLength(16, 1)
	val MP3_SIDEINFO_SC_SCFSI_BAND3 =		new OffsetAndLength(17, 1)
	val MP3_SIDEINFO_SC_GRANULE0 = 			new OffsetAndLength(18, 59)
	val MP3_SIDEINFO_SC_GRANULE1 = 			new OffsetAndLength(77, 59)	

	//algorithm id
	val MPEG_1_0 = 1 // <-- MPEG 1.0 is the only algorithm supported.
	val MPEG_1_5 = 2
	
	//layers
	val LAYER_I   = 3
	val LAYER_II  = 2
	val LAYER_III = 1 // <-- Layer 3 is the only type supported.
	
	//modes
	val STEREO = 0
	val JOINT_STEREO = 1
	val DUAL_CHANNEL = 2
	val SINGLE_CHANNEL = 3
	
	
	//joint stereo types
/*	mono
	stereo
	dual channel
	jstereo w/o intensity
	jstereo w/ intensity
	msstereo w/o intensity
	msstereo w/ intensity
*/
	//block type
	val INVALID = -1
	val NORMAL = 0
	val START_BLOCK = 1
	val SHORT_WINDOWS = 2
	val END_BLOCK = 3

	def bitrate(bitrateIndex: Int): Option[Int] = BITRATES_V1_0.get(bitrateIndex)	
	def sampleRate(samplerateIndex: Int): Option[Int] = SAMPLE_RATES_V1_0.get(samplerateIndex)
	
	val SAMPLE_RATES_V1_0 = Map(0 -> 44100,
								1 -> 48000,
								2 -> 32000)
	
	val BITRATES_V1_0 = Map(1 -> 32000,
						    2 -> 40000,
						    3 -> 48000,
						    4 -> 56000,
						    5 -> 64000,
						    6 -> 80000,
						    7 -> 96000,
						    8 -> 112000,
						    9 -> 128000,
						   10 -> 160000,
						   11 -> 192000,
						   12 -> 224000,
						   13 -> 256000,
						   14 -> 320000)	
	
	val MAX_SCFB_LONG_WINDOWS = 20 	//the max scalefactor band for long windows (0..20)
	val MAX_SCFB_SHORT_WINDOWS = 11 //the max scalefactor band for short windows (0..11)
	
	val NO_SCFSI: Array[Boolean] = Array(false,false,false,false)

	case class StereoMode(stereoMode: Int, jointStereoMode: Int) {
	  def nChannels = if (stereoMode == SINGLE_CHANNEL) 1 else 2
	  def isStereo = (nChannels == 2)
	  def isMono = (nChannels == 1)
	  def singleChannel = (nChannels == 1)
	  def isJointStereo = (stereoMode == JOINT_STEREO)
	  def intensityOn = ((jointStereoMode & 0x01) != 0)
	  def isMiddleSide = ((jointStereoMode & 0x02) != 0)
	  
	  override def toString = stereoMode match {
	    case SINGLE_CHANNEL => "[SINGLE  CH]"
	    case DUAL_CHANNEL =>   "[ DUAL  CH ]"
	    case STEREO =>         "[  STEREO  ]"
	    case JOINT_STEREO =>   "[J-STEREO "+jointStereoMode+"]"
	  }
	}
}