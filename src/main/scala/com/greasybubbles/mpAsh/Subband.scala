package com.greasybubbles.mpAsh

import scala.collection.mutable.ArrayBuffer
import com.greasybubbles.mpAsh.Mp3Constants._

class TimeDomainSubband(val samples: Array[Double])

class Subband(val samples: Array[Double], val blockType: Int) {
  def samplesWindow0(i: Int) = if (blockType == SHORT_WINDOWS) samples(i*3 + 0) else 0
  def samplesWindow1(i: Int) = if (blockType == SHORT_WINDOWS) samples(i*3 + 1) else 0
  def samplesWindow2(i: Int) = if (blockType == SHORT_WINDOWS) samples(i*3 + 2) else 0
  
  def toTimeDomain = blockType match {
    case SHORT_WINDOWS => new TimeDomainSubband(Imdct.windowedImdct(Array.tabulate(6)(samplesWindow0), Array.tabulate(6)(samplesWindow1), Array.tabulate(6)(samplesWindow2))) // short window imdct
    case _ => new TimeDomainSubband(Imdct.windowedImdct(samples, blockType))
  }
}


object Subband {

	def sum(first: Int, last: Int, f: Int => Double): Double = {
	  var sum = 0.0
	  var i = first
	  while (i<=last) {
	    sum += f(i)
	    i += 1
	  }
	  sum
	}
  
  def aliasReduction(v: Array[Double]): Array[Double] = {
    def butterflyOperation(coefs: Array[Double], i: Int): Double = {
      val subbandInQuestion: Int = i/18
      val indexWithinSubband: Int = i%18
      val bottomHalfOfSubband = (indexWithinSubband < 9)
      val topHalfOfSubband = !bottomHalfOfSubband
      val butterflyIndex = if (bottomHalfOfSubband) indexWithinSubband else 17-indexWithinSubband
      val isThisCoefficientAffectedAtAll = {
        if ((indexWithinSubband == 8 || indexWithinSubband == 9) ||
            (subbandInQuestion == 0 && bottomHalfOfSubband) ||
            (subbandInQuestion == 31 && topHalfOfSubband)) false else true
      }
      if (!isThisCoefficientAffectedAtAll)
        coefs(i)
      else if (bottomHalfOfSubband)
        coefs(subbandInQuestion*18 + butterflyIndex)*cs(butterflyIndex) + coefs(subbandInQuestion*18 - butterflyIndex - 1)*ca(butterflyIndex)
      else
        - coefs((subbandInQuestion + 1)*18 + butterflyIndex)*ca(butterflyIndex) + coefs((subbandInQuestion+1)*18 - butterflyIndex - 1)*cs(butterflyIndex)
    }
    Array.tabulate(576)(butterflyOperation(v,_))
  }
  
  // tables for alias reduction butterfly calculation
  val c: Array[Double] = Array(-0.6, -0.535, -0.33, -0.185, -0.095, -0.041, -0.0142, -0.0037)
  def _cs(i: Int) = 1.0d / Math.sqrt(1.0d + c(i)*c(i))
  def _ca(i: Int) = c(i) / Math.sqrt(1.0d + c(i)*c(i))
  val cs: Array[Double] = Array.tabulate(8)(_cs)
  val ca: Array[Double] = Array.tabulate(8)(_ca)
}
