package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

class ShortWindowBlock(si: BlockSideInfo, md: BlockMainData)
		extends Block(si: BlockSideInfo, md: BlockMainData, NO_SCFSI) {

  override def freqLine(i: Int) = freqLines(i)
  override def scalefactors(i: Int) = scalefactors3(i % 3)(i / 3)

  private lazy val freqLines = Array.tabulate(576)(i => requantizedFreqLines(i % 3, i / 3))
  
	//returns the length in bits for the scalefactor number n (it's variable)
	//based on the other parameters contained in the granule side info.
	def scalefactorLength(n: Int): Option[Int] = {
	  if (si.bsSwitchPoint == 0) { 
		  if (n >= 0 && n <= 5) Some(si.slen1)
		  else if (n >= 6 && n <= 11) Some(si.slen2)
		  else None
	  } else { //switchPoint == 1
	      throw new Exception("Switched blocks not supported yet!")
	      None
	  }
	}
	
	//returns the offset in bits (with respect to the beginning of the scalefactors)
	//at which the scalefactor number n begins for the window w
	def scalefactorOffset(n: Int, w: Int): Option[Int] = {
      if (si.bsSwitchPoint == 1) throw new Exception("Switched blocks not supported yet!")
      else if ((n < 0 || n > MAX_SCFB_SHORT_WINDOWS) || (w < 0 || w > 2)) None
      else if (n == 0) Some(0 + scalefactorLength(n).get * w)
	  else Some(scalefactorOffset(n-1, 0).get + scalefactorLength(n-1).get * 3 + scalefactorLength(n).get * w )
	}

	def scalefactor(n: Int, w: Int): Option[Int] = {
	  if (n == 12) Some(0) //fictitious scalefactor band to cover top of freq range
	  else {
		val offset = scalefactorOffset(n, w).get
		val length = scalefactorLength(n).get
    	if (length == 0) Some(0)
	    else Some(scalefactorBits.getUint(offset, length))
	  }
	}

	lazy val scalefactors3 = Seq( Seq.tabulate(13)(scalefactor(_,0).get), 
								  Seq.tabulate(13)(scalefactor(_,1).get),
								  Seq.tabulate(13)(scalefactor(_,2).get) )
	
								  	
	def requantizedFreqLines(window: Int, freqline: Int) = {
	    val scalefactorBand = ScalefactorBand.getBandFor(freqline * 3, true).get
	    val freqLineIndex = ScalefactorBand.band(scalefactorBand, 44100, true).get.bottom + 
	    		ScalefactorBand.band(scalefactorBand, 44100, true).get.width/3 * window + 
	    		(freqline - ScalefactorBand.band(scalefactorBand, 44100, true).get.bottom/3)
		val is = rawCoefficients(freqLineIndex) //huffman decoded value for index i
		val thisScalefactor: Int = scalefactors3(window)(scalefactorBand)
		val exponent: Int = (si.globalGain - 210) -
		                    8*si.subblockGain(window) -
		                    (if (si.scalefacScale == 1) 4 else 2) * (thisScalefactor)
		Math.signum(is) * Block.fourThirdsPowerTable(Math.abs(is)) * Math.pow(2, 0.25d*exponent)
		//Math.signum(is) * Math.pow(Math.abs(is), 4.0/3.0) * Math.pow(2, 0.25d*exponent)
	}

}

// SCALEFACTORS for UNSWITCHED SHORT BANDS order: (number, (Short/Long), Window)
//  0S0  0S1  0S2 
//  1S0  1S1  1S2 
//  2S0  2S1  2S2 
//  3S0  3S1  3S2 
//  4S0  4S1  4S2 
//  5S0  5S1  5S2 
//  6S0  6S1  6S2 
//  7S0  7S1  7S2 
//  8S0  8S1  8S2 
//  9S0  9S1  9S2 
// 10S0 10S1 10S2 
// 11S0 11S1 11S2 

// SCALEFACTORS for SWITCHED SHORT BANDS order: (number, (Short/Long), Window)
// 0L  1L  2L  3L
// 4L  5L  6L  7L
//  2S0  2S1  2S2 
//  3S0  3S1  3S2 
//  4S0  4S1  4S2 
//  5S0  5S1  5S2 
//  6S0  6S1  6S2 
//  7S0  7S1  7S2 
//  8S0  8S1  8S2 
//  9S0  9S1  9S2 
// 10S0 10S1 10S2 
// 11S0 11S1 11S2 
