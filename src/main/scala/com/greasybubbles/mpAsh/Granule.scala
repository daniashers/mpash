package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

abstract class IGranule(val ch0: IBlock, val ch1: IBlock, stereoMode: StereoMode) {
	def leftSubband(i: Int): Subband
	def rightSubband(i: Int): Subband
}

class Granule(ch0: IBlock, ch1: IBlock, stereoMode: StereoMode) extends IGranule(ch0: IBlock, ch1: IBlock, stereoMode: StereoMode) {
  override def toString = {
    val header = "Block type  bits(scf) gain em sc region_addr big_values one zero   h.tables"
    val line1 = if (stereoMode.nChannels == 1) ch0.toString.replace("BLOCK", "[ M ]")
    			else if (stereoMode.isMiddleSide) ch0.toString.replace("BLOCK", "[L+R]")
    			else ch0.toString.replace("BLOCK", "[ L ]")
    val line2 = if (stereoMode.nChannels == 1) ""
    			else if (stereoMode.isMiddleSide) ch1.toString.replace("BLOCK", "[L-R]")
    			else ch1.toString.replace("BLOCK", "[ R ]")
    header + "\n" + line1 + "\n" + line2
  }
  
  override def leftSubband(i: Int) = stereoMode.stereoMode match {
	  case SINGLE_CHANNEL => ch0.subband(i)
	  case DUAL_CHANNEL => ch0.subband(i)
	  case STEREO => ch0.subband(i)
	  case JOINT_STEREO => {
	    if (stereoMode.intensityOn || stereoMode.isMiddleSide) StereoProcessing.processSubband(ch0, ch1, stereoMode, false, i)
	    else ch0.subband(i)
	  }
	  case _ => null
	}
	
	override def rightSubband(i: Int) = stereoMode.stereoMode match {
	  case SINGLE_CHANNEL => ch0.subband(i)
	  case DUAL_CHANNEL => ch1.subband(i)
  	  case STEREO => ch1.subband(i)
	  case JOINT_STEREO => {
	    if (stereoMode.intensityOn || stereoMode.isMiddleSide) StereoProcessing.processSubband(ch0, ch1, stereoMode, true, i)
	    else ch1.subband(i)
	  }
	  case _ => null
	}
}