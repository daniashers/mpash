package com.greasybubbles.mpAsh

import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.DataLine
import javax.sound.sampled.LineUnavailableException
import javax.sound.sampled.SourceDataLine
import javax.sound.sampled.DataLine.Info

class AudioPort(nrChannels: Int, sampleRate: Int, bytesPerSample: Int, bufferLengthMillis: Int) {
  private var line: SourceDataLine = null

  val bufferLengthBytes: Int = nrChannels * bytesPerSample * sampleRate * bufferLengthMillis / 1000;

  val audioFormat: AudioFormat = new AudioFormat(sampleRate, bytesPerSample * 8, nrChannels, true, false);

  val info: Info = new Info(classOf[DataLine], audioFormat);

  try {
    line = AudioSystem.getLine(info).asInstanceOf[SourceDataLine];
    line.open(audioFormat, bufferLengthBytes);
  } catch {
    case e: LineUnavailableException => throw new RuntimeException("Unable to open audio line.", e);
  }
  assert (line.getBufferSize() == bufferLengthBytes);
  line.start();

  def playRaw(samples: Array[Byte]) {
    line.write(samples, 0, samples.length)
  }

  def playLR16(left: Seq[Int], right: Seq[Int]) {
    assert(left.length == right.length);
    val byteStream = new Array[Byte]((left.length + right.length) * 2);
    for (i <- 0 until left.length) {
      val samplel = left(i) max -32768 min 32767;
      val sampler = right(i) max -32768 min 32767;
      byteStream(i*4+0) = (samplel & 0xFF).toByte;
      byteStream(i*4+1) = ((samplel >> 8) & 0xFF).toByte;
      byteStream(i*4+2) = (sampler & 0xFF).toByte;
      byteStream(i*4+3) = ((sampler >> 8) & 0xFF).toByte;
    }
    playRaw(byteStream);
  }

  def readyFor(nrSamples: Int): Boolean = {
    (line.available > nrSamples * bytesPerSample * nrChannels)
  }

  def endAudio: Unit = { /* audio port cannot be used after calling this, a new one must be obtained */
    line.drain()
    line.stop()
  }
}
