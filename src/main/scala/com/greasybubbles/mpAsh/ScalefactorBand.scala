package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

class ScalefactorBand(val n: Int, val bottom: Int, val top: Int, val slen: Boolean) {
  def width = top - bottom + 1
}

object ScalefactorBand {
	private def selectTable(sampleRate: Int, isShortWindows: Boolean): Array[ScalefactorBand] = isShortWindows match {
	  case false => SCALEFACTOR_44100_LONG
	  case true => SCALEFACTOR_44100_SHORT_NONSWITCHED
	  //TODO switched case...
	}
	def preEmphasisFactor: Array[Int] = Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 3, 3, 3, 2)
	def getBandFor(i: Int, shortWindows: Boolean): Option[Int] = {
		val table: Array[ScalefactorBand] = selectTable(44100, shortWindows)
		@annotation.tailrec
		def go(j: Int): Option[Int] = {
		    if (table(j).bottom <= i && i <=table(j).top) Some(j) else if (j == 0) None else go(j-1)
		}
	    go(table.size-1)
	}
	def band(i: Int, sampleFreq: Int, shortWindows: Boolean): Option[ScalefactorBand] = {
		val table: Array[ScalefactorBand] = selectTable(sampleFreq, shortWindows)
		Some(table(i))
	}  
	
	// RAW DATA TABLES

    val SCALEFACTOR_44100_LONG: Array[ScalefactorBand] = Array(
        // (number, bottom, top)
		new ScalefactorBand(0, 0, 3, false),
		new ScalefactorBand(1, 4, 7, false),
		new ScalefactorBand(2, 8, 11, false),
		new ScalefactorBand(3, 12, 15, false),
		new ScalefactorBand(4, 16, 19, false),
		new ScalefactorBand(5, 20, 23, false),
		new ScalefactorBand(6, 24, 29, false),
		new ScalefactorBand(7, 30, 35, false),
		new ScalefactorBand(8, 36, 43, false),
		new ScalefactorBand(9, 44, 51, false),
		new ScalefactorBand(10, 52, 61, false),
		new ScalefactorBand(11, 62, 73, true),
		new ScalefactorBand(12, 74, 89, true),
		new ScalefactorBand(13, 90, 109, true),
		new ScalefactorBand(14, 110, 133, true),
		new ScalefactorBand(15, 134, 161, true),
		new ScalefactorBand(16, 162, 195, true),
		new ScalefactorBand(17, 196, 237, true),
		new ScalefactorBand(18, 238, 287, true),
		new ScalefactorBand(19, 288, 341, true),
 		new ScalefactorBand(20, 342, 417, true),
 		new ScalefactorBand(21, 418, 575, true) //this last band is a 'fictitious' band just to cover the top few frequency lines...
    )
    val SCALEFACTOR_44100_SHORT_NONSWITCHED: Array[ScalefactorBand] = Array(
        // (number, bottom, top)
        new ScalefactorBand(0, 0, 11, false),
		new ScalefactorBand(1, 12, 23, false),
		new ScalefactorBand(2, 24, 35, false),
		new ScalefactorBand(3, 36, 47, false),
		new ScalefactorBand(4, 48, 65, false),
		new ScalefactorBand(5, 66, 89, false),
		new ScalefactorBand(6, 90, 119, true),
		new ScalefactorBand(7, 120, 155, true),
		new ScalefactorBand(8, 156, 197, true),
		new ScalefactorBand(9, 198, 251, true),
		new ScalefactorBand(10, 252, 317, true),
		new ScalefactorBand(11, 318, 407, true),
        new ScalefactorBand(12, 408, 575, true) //fictitious band
    )
    val SCALEFACTOR_44100_SHORT_SWITCHED: Array[ScalefactorBand] = Array( // TO FIX
        // (number, bottom, top)
		new ScalefactorBand(0, 0, 3, false),
		new ScalefactorBand(1, 4, 7, false),
		new ScalefactorBand(2, 8, 11, false),
		new ScalefactorBand(3, 12, 15, false),
		new ScalefactorBand(4, 16, 19, false),
		new ScalefactorBand(5, 20, 23, false),
		new ScalefactorBand(6, 24, 29, false),
		new ScalefactorBand(7, 30, 35, false),
		// TODO BROKEN!
		new ScalefactorBand(8, 12, 15, false),
		new ScalefactorBand(9, 16, 21, false),
		new ScalefactorBand(10, 22, 29, false),
		new ScalefactorBand(11, 30, 39, true),
		new ScalefactorBand(12, 40, 51, true),
		new ScalefactorBand(13, 52, 65, true),
		new ScalefactorBand(14, 66, 83, true),
		new ScalefactorBand(15, 84, 105, true),
		new ScalefactorBand(16, 106, 135, true)
    )

    //returns whether, based on the scfsi array (see documentation), the given scalefactor band
    //is transmitted for this granule or whether the values should be reused from the previous granule.
    //Basically if the value is TRUE for scfsi for the current group of bands, it means it is not transmitted.
    def isTransmitted(b: Int, scfsi: Array[Boolean]): Boolean = {
		  // val SCFSI_BAND_0: Array[Int] = Array(0,1,2, 3, 4, 5)
		  // val SCFSI_BAND_1: Array[Int] = Array( 6, 7, 8, 9,10)
		  // val SCFSI_BAND_2: Array[Int] = Array(11,12,13,14,15)
		  // val SCFSI_BAND_3: Array[Int] = Array(16,17,18,19,20)
		  if (b < 0) false
		  else if (b <= 5) !scfsi(0)
		  else if (b <= 10) !scfsi(1)
		  else if (b <= 15) !scfsi(2)
		  else if (b <= 20) !scfsi(3)
		  else false
	}
	
	
	def slen1(scalefacCompress: Int): Int = scalefacCompress match {
	  case  0 => 0
	  case  1 => 0
	  case  2 => 0
	  case  3 => 0
	  case  4 => 3
	  case  5 => 1
	  case  6 => 1
	  case  7 => 1
	  case  8 => 2
	  case  9 => 2
	  case 10 => 2
	  case 11 => 3
	  case 12 => 3
	  case 13 => 3
	  case 14 => 4
	  case 15 => 4
	}
	
	def slen2(scalefacCompress: Int): Int = scalefacCompress match {
	  case  0 => 0
	  case  1 => 1
	  case  2 => 2
	  case  3 => 3
	  case  4 => 0
	  case  5 => 1
	  case  6 => 2
	  case  7 => 3
	  case  8 => 1
	  case  9 => 2
	  case 10 => 3
	  case 11 => 1
	  case 12 => 2
	  case 13 => 3
	  case 14 => 2
	  case 15 => 3
	}
	
}