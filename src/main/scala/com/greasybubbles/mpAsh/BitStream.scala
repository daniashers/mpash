package com.greasybubbles.mpAsh

import scala.collection.mutable.StringBuilder

object BitStream {
  def literalStream(pattern: String): BitStream = {
    val literalData = new Array[Byte](pattern.length()/8+1)
    for (i <- 0 to pattern.length()-1) {
      if (pattern.charAt(i).equals('1')) {
    	  literalData(i/8) = (literalData(i/8) | (1 << (7-i%8))).toByte
      }
    }
    val baseStream = new PrimaryBitStream(literalData)
    baseStream.sub(0,pattern.length())
  }
   
  def EMPTY: BitStream = new PrimaryBitStream(new Array[Byte](0))
  
  def fromInt(v: Int, bitlen: Int): BitStream = {
    var generatorString: String = ""
    for (i <- 0 to bitlen-1) {
      generatorString = generatorString + (if ((v & 1<<(bitlen-i-1)) != 0) "1" else "0")
    }
    BitStream.literalStream(generatorString)
  }
  
  
    def contains(seq: BitStream, ptn: BitStream): Boolean = {
    	var win = false
    	for (i <- 0 to (seq.len-1 - (ptn.len-1))) {
    		var matched = true
    		for (j <- 0 to (ptn.len-1)) {
    			if (seq.bit(i+j) != ptn.bit(j)) matched = false
    		}
    		if (matched) win = true
    	}
    	win
    }

    @annotation.tailrec
    def isAllZero(seq: BitStream): Boolean = {
    	if (seq.len == 0) true
    	else if (seq.head != false) false
    	else isAllZero(seq.tail)
    }
    

}

abstract class BitStream {
  //Abstract
  def bit(offset: Int): Boolean
  def len: Int
  //Default
  def head = bit(0)
  def tail = this.sub(1,len-1)
  
  override def toString: String = {
    var str: StringBuilder = new StringBuilder();
    for (i <- 0 to this.len-1) {
      str.append(if (this.bit(i)) "1" else "0")
    }
    str.toString()
  }

  def getUint(offset: Int, width: Int): Int = {
    @annotation.tailrec
    def go(accu: Int, bitPos: Int): Int = {
      if (bitPos == width) accu
      else go(accu*2 + (if (bit(offset+bitPos)) 1 else 0), bitPos+1)
    }
    go(0,0)
  }

  def getUint(oal: OffsetAndLength): Int = getUint(oal.offset, oal.length)
  
  def equals(b: BitStream): Boolean = {
    @annotation.tailrec
    def equals_loop(a: BitStream, b: BitStream): Boolean = {
      if (a.len == 0) true
      else if (a.head != b.head) false
      else equals_loop(a.tail, b.tail)
    }
    if (b.len != this.len) false
    else equals_loop(b,this)
  }
  
  def +(other: BitStream): BitStream = new SeamlessBitStream(this, other)
  def sub(offset: Int, length: Int): BitStream = new LogicalBitStream(this, offset, length)
}

class LogicalBitStream(base: BitStream, start: Int, length: Int) extends BitStream {
  if (start+length > base.len) throw new IndexOutOfBoundsException("Attempting to create substream %d..%d but base stream length %d".format(start, start+length, base.len))
  override def len = this.length
  override def bit(pos: Int): Boolean = {
    if (pos < 0 || pos > len-1) throw new IndexOutOfBoundsException("Index requested %d but stream length %d".format(pos,len))
    else base.bit(start+pos)
  }
  def getParent = base
  //overridden for performance reasons to avoid nesting substreaming too deep. Instead we re-derive from the parent
  override def sub(offset: Int, length: Int): BitStream = new LogicalBitStream(base, start+offset, length)
}

class PrimaryBitStream(data: Array[Byte]) extends BitStream {
  override def bit(offset: Int): Boolean = {
    if (offset < 0 || offset > len-1) throw new IndexOutOfBoundsException("Index requested %d but stream length %d".format(offset,len))
    else ( (data(offset/8) & (1<<(7-offset%8)).toByte) != 0 )
  }
  override def len = data.length*8
}

class SeamlessBitStream(bs1: BitStream, bs2: BitStream) extends BitStream {
  override def len = bs1.len + bs2.len
  override def bit(offset: Int) = {
    if (offset < 0 || offset > len-1) throw new IndexOutOfBoundsException("Index requested %d but stream length %d".format(offset,len))
    else if (offset < bs1.len) bs1.bit(offset)
    else bs2.bit(offset - bs1.len)
  }
}

class ReversedStream(bs: BitStream) extends BitStream {
  override def len = bs.len
  override def bit(offset: Int) = {
    if (offset < 0 || offset > len-1) throw new IndexOutOfBoundsException("Index requested %d but stream length %d".format(offset,len))
    else bs.bit(bs.len - offset - 1)
  }
}