package com.greasybubbles.mpAsh

object Mp3DisplayUtil {
	
  def getDisplayString(frm: Frame, synthesisResult: SynthesisResult): String = {
    var output = new StringBuilder()
	
	output.append(frm.toString + "\n")
	output.append(frm.granule0 + "\n")
	output.append(frm.granule1 + "\n")
	output.append("peak " + drawBar(peak(synthesisResult.pcmLeft ).toFloat/32768f, 32, '_', ' ') + "   sub- " + spectrumTop(synthesisResult.spectrum._1) + "\n") 
	output.append("RMS  " + drawBar(rms (synthesisResult.pcmLeft ).toFloat/32768f, 32, '|', ' ') + "]  band " + spectrumBottom(synthesisResult.spectrum._1) + "\n")
	output.append("peak " + drawBar(peak(synthesisResult.pcmRight).toFloat/32768f, 32, '_', ' ') + "  power " + spectrumTop(synthesisResult.spectrum._2) + "\n")
	output.append("RMS  " + drawBar(rms (synthesisResult.pcmRight).toFloat/32768f, 32, '|', ' ') + "]  0-31 " + spectrumBottom(synthesisResult.spectrum._2) + "\n")
	output.append("Stream parsing           : %5.2f ms  ".format(synthesisResult.benchmarkData.unpackingTime.toFloat/1000000) + "\n")
	output.append("Huffman & dequantisation : %5.2f ms  ".format(synthesisResult.benchmarkData.huffmanTime.toFloat/1000000) + "\n")
	output.append("Stereo processing        : %5.2f ms  ".format(synthesisResult.benchmarkData.stereoTime.toFloat/1000000) + "\n")			  
	output.append("Inverse cosine transform : %5.2f ms  ".format(synthesisResult.benchmarkData.imdctTime.toFloat/1000000) + "\n")
	output.append("Synthesis filterbank     : %5.2f ms  ".format(synthesisResult.benchmarkData.filterbankTime.toFloat/1000000) + "\n")
	output.toString

  }
  
    def drawBar(value: Float, width: Int, f: Char = '|', e: Char = '.'): String = {
    val fs: Int = Math.min(Math.round(value * width).toInt, width)
    var b: String = ""
    for (i <- 1 to fs) b = b + f
    for (i <- 1 to width - fs) b = b + e
    val bar = b
    bar
  }

  def rms(samples: Array[Int]): Int = {
    Math.sqrt(samples.foldLeft[Double](0.0)((accu, s) => accu + s*s) / samples.length).toInt
  }
  
  def peak(samples: Array[Int]): Int = {
    samples.foldLeft(0)((z:Int, i:Int) => Math.max(z,Math.abs(i))).toInt
  }
  
  def spectrumTop(bands: Array[Double]): String = {
    def rule(v: Double): String = {
      if (v > 0.1) "`"
   	  else if (v > 0.04) "-"
      else if (v > 0.01) "."
      else if (v > 0.005) "_"
   	  else " "
    }
    bands.map(rule(_)).mkString("")
  }
  
  def spectrumBottom(bands: Array[Double]): String = {
    def rule(v: Double): String = {
      if (v > 0.005) " "
      else if (v > 0.002) "`"
      else if (v > 0.001) "-"
      else if (v > 0.0005) "."
      else "_"    	
    }
    bands.map(rule(_)).mkString("")
  }
}