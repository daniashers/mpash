package com.greasybubbles.mpAsh

import java.io.FileInputStream;
import java.io.File;

import scala.collection.mutable.ArrayBuffer
import com.greasybubbles.mpAsh.Mp3Constants._
import org.fusesource.jansi.AnsiConsole
import org.fusesource.jansi.Ansi._
import scala.collection.mutable.Queue
import scala.util.control.Breaks._

object Mp3Main {
  
    val ansiOut = AnsiConsole.out
    def print(s: Any): Unit = ansiOut.print(s)
    def println(s: Any): Unit = ansiOut.println(s)

    var visOutput = new StringBuilder()
    
	def main(args: Array[String]): Unit = {

    	//INIT
      
	    var fileContent: Array[Byte] = null
	    try {
			val mp3File: File = new File(args(0))
			val mp3Stream: FileInputStream = new FileInputStream(mp3File)
			fileContent = new Array[Byte](mp3File.length.toInt)
			mp3Stream.read(fileContent)
	    } catch {
	      case _: Throwable => println("Error reading file!")
	    }
		val stream: BitStream = new PrimaryBitStream(fileContent)
		
		//sets up the printing area
		println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
		print(ansi.cursorUp(20))
		print(ansi.saveCursorPosition())

		val bufferLengthMillis = 1000

		val audioOut = new AudioPort(nrChannels = 2, sampleRate = 44100, bytesPerSample = 2, bufferLengthMillis = bufferLengthMillis)

		val nrBufferableFrames = Math.round(bufferLengthMillis / 1000.0 / (1152.0/44100.0 /* length of a frame */)).toInt

		//sets up a 'delay' line for the visualisation effects. This is needed because, due to the fact
		//that the audio is buffered, there would be a delay between the visual effect and the actual sound.
		var visDelayLine = new Queue[String]()
		for (i <- 1 to nrBufferableFrames) {
		  visDelayLine.enqueue("")
		}
		val nrSamplesPerFrame = 1152
		while (audioOut.readyFor(nrSamplesPerFrame)) { //fills the buffer with silence
		  audioOut.playLR16(Array.fill(nrSamplesPerFrame)(0), Array.fill(nrSamplesPerFrame)(0))
		}
		
		//main loop

		@annotation.tailrec
		def playLoop(
		    stream: BitStream, 
		    frameNumber: Int = 1, 
		    position: Long = 0, 
		    reservoir: BitStream = BitStream.EMPTY,
		    synthState: RollingSynthData = RollingSynthData.empty): Unit = {

		  val frm: Frame = try {
		    Frame.getNextFromStream(stream, position.toInt, reservoir).get
		  } catch {
		    case _: Throwable => null
		  }
		  
		  if (frm != null) {
			  val t0 = System.nanoTime() 
			  val synthesisResult = SynthesizerChain.synthesizeFrame(frm, synthState)
			  val t1 = System.nanoTime() 
			  
			  val us: Int = (t1-t0).toInt/1000
			  val realtimeUs = 26122
			  
			  val totalSeconds = (frameNumber.toFloat * 1152 / frm.sampleRate.get).toInt 
			  val minutes = totalSeconds / 60
			  val seconds = totalSeconds % 60
	
			  visOutput.clear()
			  visOutput.append("Frame %5d - %3d:%02d - ".format(frameNumber, minutes, seconds))
			  visOutput.append(Mp3DisplayUtil.getDisplayString(frm, synthesisResult) + "\n")
			  visOutput.append("Frame decoded in %6.2f ms (%4.1fx realtime)\n".format(us.toFloat/1000,realtimeUs.toFloat/us))
			  //the following two lines is the delay line in operation.
			  //As long as it is pre-filled with the desired number of frames to delay prior to use,
			  //it will have the effect of delaying as requested...
			  
			  visDelayLine.enqueue(visOutput.toString)
			  
			  print(ansi.restorCursorPosition())
			  print(visDelayLine.dequeue)
			  
			  while(!audioOut.readyFor(1152)) {
			    //wait
			  }
			  audioOut.playLR16(synthesisResult.pcmLeft, synthesisResult.pcmRight)
			  
			  playLoop(stream, frameNumber + 1, frm.startOfNextFrame, frm.leftoverBits, synthesisResult.rollingSynthData)
		  }	
		}
		
		def flushDelayLine: Unit = {
		  while (!audioOut.readyFor(1152)) {
		    //wait
		  }
		  audioOut.playLR16(Array.fill[Int](1152)(0), Array.fill[Int](1152)(0)) // play silence
		  print(ansi.restorCursorPosition())
		  print(visDelayLine.dequeue)
		  if (!visDelayLine.isEmpty) flushDelayLine
		}
		
		playLoop(stream)
		flushDelayLine
		println("End of stream reached.")
		audioOut.endAudio
	}
}