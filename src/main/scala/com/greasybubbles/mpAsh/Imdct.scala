package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

object Imdct {
  
	// IMDCT for short windows
	// fds (frequency domain samples): size 12 each
	// output: 36 time-domain samples
	def windowedImdct(fds0: Array[Double], fds1: Array[Double], fds2: Array[Double]): Array[Double] = {
		val timeSamples0: Array[Double] = Array.tabulate(12)(imdct12(fds0, _))
		val timeSamples1: Array[Double] = Array.tabulate(12)(imdct12(fds1, _))
		val timeSamples2: Array[Double] = Array.tabulate(12)(imdct12(fds2, _))

		def overlappedSample(i: Int): Double = {
		    if (i < 6)		  0
		    else if (i < 12)  timeSamples0(i- 6) * z2(i- 6)
		    else if (i < 18)  timeSamples0(i- 6) * z2(i- 6) + timeSamples1(i-12) * z2(i-12)
		    else if (i < 24)  timeSamples1(i-12) * z2(i-12) + timeSamples2(i-18) * z2(i-18)
		    else if (i < 30)  timeSamples2(i-18) * z2(i-18)
		    else 			  0	  
		}
		
		Array.tabulate(36)(overlappedSample(_))
	}
	
	// use this for long windows. (this is the 'public' function.)
	// samples in size 36, samples out size 36
	def windowedImdct(freqDomainSamples: Array[Double], blockType:Int): Array[Double] = {
	  Array.tabulate(36)(windowedImdctLongWindows(freqDomainSamples, _, blockType))
	}
	
	// previous Samples: length 36
	// current Samples: length 36
	def overlap(previous: TimeDomainSubband, current: TimeDomainSubband): Array[Double] =
			Array.tabulate(18)(i => previous.samples(i+18) + current.samples(i))
	

		// samples in: 18 frequency-domain samples
	// out: one of 36 time-domain samples
	private def imdct36(freqDomainSamples: Array[Double], i: Int): Double = {
	    val n = 36
	    var accu: Double = 0
	    var k: Int = 0
	    while (k < n/2) {
	      val cosArg: Int = Math.abs((2*i + 1 + n/2) * (2*k +1))
	      accu = accu + freqDomainSamples(k) * cosTable(cosArg  % 144)
	      k = k+1
	    }
	    accu 
	}
	
	// samples in: 6 frequency-domain samples
	// out: one of 12 time-domain samples
	private def imdct12(freqDomainSamples: Array[Double], i: Int): Double = {
	    val n= 12
	    var accu: Double = 0
	    var k: Int = 0
	    while (k < n/2) {
	      val cosArg: Int = Math.abs((2*i + 1 + n/2) * (2*k + 1))
	      accu = accu + freqDomainSamples(k) * cosTable(cosArg * 3  % 144)
	      k = k + 1
	    }
	    accu
	}
	
	// freqDomainSamples: size 36
	private def windowedImdctLongWindows(freqDomainSamples: Array[Double], i: Int, blockType: Int): Double = {
	  // first, select the appropriate windowing function
	  val z: (Int => Double) = blockType match {
	    case 0 => z0
	    case 1 => z1
	    case 3 => z3
	  }	  
	  imdct36(freqDomainSamples, i) * z(i) //z: windowing function
	}

	private val cosTable: Array[Double] = Array.tabulate(144)(i => Math.cos(Math.PI/(2*36)*i))
	
	// IMDCT windowing function (called 'z' in the specification)
	private def _z(blockType: Int): (Int => Double) = (i: Int) => blockType match {
		case 0 => Math.sin(Math.PI/36 * (i + 0.5f))
		case 1 => if (i < 18) Math.sin(Math.PI/36 * (i + 0.5f))
				  else if (i < 24) 1
				  else if (i < 30) Math.sin(Math.PI/12 * (i - 18 + 0.5f))
				  else 0
		case 2 => Math.sin(Math.PI/12 * (i + 0.5f)) // Short window
		case 3 => if (i < 6) 0
				  else if (i < 12) Math.sin(Math.PI/12 * (i - 6 + 0.5f))
				  else if (i < 18) 1
				  else Math.sin(Math.PI/36 * (i + 0.5f))
	}
	
	// Windowing function for normal block type (0). Interval [0, 36)
	private lazy val z0: Seq[Double] = Seq.tabulate(36)(_z(0))
	// Windowing function for 'start' block type (1). Interval [0, 36)
	private lazy val z1: Seq[Double] = Seq.tabulate(36)(_z(1))
	// Windowing function for 'short windows' block type (2). Interval [0, 12)
	private lazy val z2: Seq[Double] = Seq.tabulate(12)(_z(2))
	// Windowing function for 'end' block type (3). Interval [0, 36)
	private lazy val z3: Seq[Double] = Seq.tabulate(36)(_z(3))
}