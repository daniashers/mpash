package com.greasybubbles.mpAsh
import scala.collection.mutable.ArrayBuffer

class RollingSynthData(
    val vl: Array[Double],		//the v-vector needed by the polyphase filterbank
    val vr: Array[Double],
    val ol: Array[TimeDomainSubband], //the portions of the imdct results to overlap with the new ones
    val or: Array[TimeDomainSubband])

object RollingSynthData {
  def empty = new RollingSynthData(
      	Array.fill(1024)(0.0d), 
		Array.fill(1024)(0.0d), 
		Array.tabulate(32)(i => new TimeDomainSubband(Array.fill(36)(0.0d))),
		Array.tabulate(32)(i => new TimeDomainSubband(Array.fill(36)(0.0d))))
}


object SynthesizerChain {

	// returns 1152 samples.
	def synthesizeFrame(f: Frame, r: RollingSynthData = RollingSynthData.empty): SynthesisResult = {

	  val synthesisResultGranule0 = SynthesizerChain.synthesizeGranule(f.granule0, r)
	  val synthesisResultGranule1 = SynthesizerChain.synthesizeGranule(f.granule1, synthesisResultGranule0.rollingSynthData)
	  
	  val aggregatedPerformanceData = synthesisResultGranule0.benchmarkData + synthesisResultGranule1.benchmarkData
	  new SynthesisResult(
	      Array.concat(synthesisResultGranule0.pcmLeft,  synthesisResultGranule1.pcmLeft),
	      Array.concat(synthesisResultGranule0.pcmRight, synthesisResultGranule1.pcmRight),      
	      synthesisResultGranule1.rollingSynthData, 
	      aggregatedPerformanceData, 
	      synthesisResultGranule1.spectrum)
	  
	}
	
	// returns 576 samples.
	def synthesizeGranule(g: IGranule, r: RollingSynthData): SynthesisResult = {

	  val t0 = System.nanoTime
	  
	  // STEP 1: force decoding of scalefactors
	  g.ch0.scalefactors(0)
	  if (g.ch1 != null) g.ch1.scalefactors(0)
	  val t1 = System.nanoTime
	  val unpackingTime = t1 - t0

	  // STEP 2: force calculation of Huffman codes
	  g.ch0.freqLine(0)
	  if (g.ch1 != null) g.ch1.freqLine(0)
  	  val t2 = System.nanoTime
	  val huffmanTime = t2 - t1

	  // STEP 3: force processing of stereo image
	  val freqDomainSubbandsL = Array.tabulate(32)(g.leftSubband(_))
	  val freqDomainSubbandsR = Array.tabulate(32)(g.rightSubband(_))
	  val t3 = System.nanoTime
	  val stereoTime = t3 - t2
	  
	  // STEP 4a: Inverse IMDCT to transform frequency domain subband into time domain subbands (length: 36 each)
	  lazy val timeDomainSubbandsL = Array.tabulate(32)(freqDomainSubbandsL(_).toTimeDomain)
	  lazy val timeDomainSubbandsR = Array.tabulate(32)(freqDomainSubbandsR(_).toTimeDomain)
	  
	  // STEP 4b: Overlap the 1st half of the new subbands with the 2nd half of the previous ones
	  val overlappedL: Array[TimeDomainSubband] = 
	    Array.tabulate(32)(i => new TimeDomainSubband(Imdct.overlap(r.ol(i), timeDomainSubbandsL(i))))
	  val overlappedR: Array[TimeDomainSubband] = 
	    Array.tabulate(32)(i => new TimeDomainSubband(Imdct.overlap(r.or(i), timeDomainSubbandsR(i))))

	  // STEP 4c: Determine the max absolute value for each subband (used in the 'spectrum analyzer' visualisation)
	  val powerL = overlappedL.map(tds => tds.samples.foldLeft(0.0d)((accu, s) => accu + s*s))
	  val powerR = overlappedR.map(tds => tds.samples.foldLeft(0.0d)((accu, s) => accu + s*s))
	  val t4 = System.nanoTime
	  val imdctTime = t4 - t3
	    
	  // STEP 5: feeds the result into the polyphase filterbank
	  var vl = r.vl
	  var vr = r.vr
	  var pcml: ArrayBuffer[Double] = new ArrayBuffer[Double](576)
	  var pcmr: ArrayBuffer[Double] = new ArrayBuffer[Double](576)
	  for (t <- 0 to 17)  { // repeat 18 times - each time it returns 32 samples.
	    val resultl = PolyphaseFilterbank.synthesize(t, overlappedL, vl)
	    val resultr = PolyphaseFilterbank.synthesize(t, overlappedR, vr)
	    vl = resultl._2
	    vr = resultr._2
	    pcml ++= (resultl._1)
	    pcmr ++= (resultr._1)
	  }	  

	  val t5 = System.nanoTime
	  val filterbankTime = t5 - t4
	  
	  // STEP 6: scales and converts the floating point values into 16-bit values
	  val pcm16l: Array[Int] = Array.tabulate(pcml.size)(i => Math.round(pcml(i).toFloat*30000)) //32768)) leave some
	  val pcm16r: Array[Int] = Array.tabulate(pcmr.size)(i => Math.round(pcmr(i).toFloat*30000)) //32768)) headroom
	  
	  val performanceData = new BenchmarkData(
	      unpackingTime,
	      huffmanTime, 
	      0L, //alias reduction time
	      stereoTime,
	      imdctTime,
	      filterbankTime)
	  
	  val spectrumData = (powerL, powerR)
	  
	  new SynthesisResult(
	      pcm16l, 
	      pcm16r, 
	      new RollingSynthData(vl, vr, timeDomainSubbandsL, timeDomainSubbandsR),
	      performanceData, 
	      spectrumData)
	}
}