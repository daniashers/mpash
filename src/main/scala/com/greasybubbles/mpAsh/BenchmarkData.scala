package com.greasybubbles.mpAsh

class BenchmarkData(
    val unpackingTime: Long,
    val huffmanTime: Long, 
    val aliasTime: Long,
    val stereoTime: Long, 
    val imdctTime: Long, 
    val filterbankTime: Long) {

  def +(other: BenchmarkData) = new BenchmarkData(
	    this.unpackingTime + other.unpackingTime,
	    this.huffmanTime + other.huffmanTime,
	    this.aliasTime + other.aliasTime,
	    this.stereoTime + other.stereoTime,
	    this.imdctTime + other.imdctTime,
	    this.filterbankTime + other.filterbankTime)
}