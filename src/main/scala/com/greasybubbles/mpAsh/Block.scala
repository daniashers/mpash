package com.greasybubbles.mpAsh

import com.greasybubbles.mpAsh.Mp3Constants._

trait IBlock {
  def freqLine(i: Int): Double		// [0, 576)
  def scalefactors(i: Int): Int		// [0, 21) for long windows, [0, 36) for short nonswitched, ? for switched (draw diagram?)
  def subband(i: Int): Subband		// [0, 32)
  
  def blockType: Int
  def isSwitched: Boolean
}

abstract class Block(val si: BlockSideInfo, val md: BlockMainData, val scfsi: Array[Boolean]) extends IBlock {
 
  val scalefactorBits: BitStream = md.sub(0, scalefactorBitsLength)
  val huffmanCodeBits: BitStream = md.sub(0 + scalefactorBitsLength, huffmanBitsLength)
  
  lazy val rawCoefficients: Array[Int] = Huffman.decodeCoefficients(this)

  override def subband(n: Int): Subband = new Subband(Array.tabulate(18)(i => freqLine(n*18 + i)), si.blockType)
  
  def blockType = si.blockType
  def isSwitched = (si.blockType == 2 && si.bsSwitchPoint == 1)

  def huffmanBitsLength: Int = si.part23Length - scalefactorBitsLength
  def scalefactorBitsLength: Int = { //aka 'part2length'
	  if (blockType != SHORT_WINDOWS) {
	    (if (!scfsi(0)) 6*si.slen1 else 0) + (if (!scfsi(1)) 5*si.slen1 else 0) + 
	    	(if (!scfsi(2)) 5*si.slen2 else 0) + (if (!scfsi(3)) 5*si.slen2 else 0) 
	  } else { //short windows
	    if (!isSwitched) 18*si.slen1 + 18*si.slen2 
	    else 17*si.slen1 + 18*si.slen2
	  }
  }
  
  
  val smallValueCount = {
    val maxNonzeroIndex = rawCoefficients.lastIndexWhere(_ != 0)
	val tempZeroCount = if (maxNonzeroIndex == -1) 576 else (575 - maxNonzeroIndex)
	val tempSmallValueCount = 576 - si.bigValues*2 - tempZeroCount
	// the small value count must always be a multiple of 4 since small values come in groups of four. So, round up:
	if (tempSmallValueCount % 4 == 0) tempSmallValueCount
	else tempSmallValueCount - (tempSmallValueCount % 4) + 4
  }
  val zeroCount = 576 - si.bigValues*2 - smallValueCount
  
  override def toString = {
      val blockTypeString: String = blockType match {
      	case 0 => "NORML"
      	case 1 => "START"
      	case 2 => if (isSwitched) "SWTCH" else "SHORT"
      	case 3 => "STOP "
      	case _ => "ERROR"
      }
      "BLOCK %s ".format(blockTypeString) + "%4d %3d  ".format(si.part23Length, scalefactorBitsLength) +
      	" %3d  %1d  %1d ".format(si.globalGain, si.preFlag, si.scalefacScale) +
      	"   %2d  %1d   ".format(si.regionAddress1, si.regionAddress2) + 
		"%3d %3d %3d %3d %3d   ".format(si.region0Count, si.region1Count, si.region2Count, smallValueCount, zeroCount) +
			"%2d/%2d/%2d/%1d".format(si.huffmanTable0, si.huffmanTable1, si.huffmanTable2, si.count1TableSelect)
  }
}

class LongWindowBlock(si: BlockSideInfo, md: BlockMainData, val previousBlock: IBlock, scfsi: Array[Boolean]) extends Block(si: BlockSideInfo, md: BlockMainData, scfsi: Array[Boolean]) {

  	override def freqLine(i: Int) = requantizedSamples(i)
	override def scalefactors(i: Int) = scfactors(i)
  
	lazy val scfactors: Array[Int] = Array.tabulate(21)(scalefactor(_))
	lazy val requantizedSamplesBeforeAliasReduction: Array[Double] = Array.tabulate(576)(requantizedSample)
	lazy val requantizedSamples: Array[Double] = Subband.aliasReduction(requantizedSamplesBeforeAliasReduction)	
	
	
	private def scalefactor(i: Int): Int = {
	  if (previousBlock == null 							//this mean this is the first granule of the frame
	  	|| ScalefactorBand.isTransmitted(i, scfsi)) {	//this is 2nd granule but this band is retransmitted
		  val offset = scalefactorOffset(i).get
		  val length = scalefactorLength(i).get
		  scalefactorBits.getUint(offset, length)
	  } else { //this is the 2nd granule and this band is NOT retransmitted: re-read from previous granule
		  previousBlock.scalefactors(i)
	  }
	}
		
	private def requantizedSample(i: Int): Double = {
		val is = rawCoefficients(i) //huffman decoded value for index i
		val sfb = ScalefactorBand.getBandFor(i, si.blockType == SHORT_WINDOWS).get min 20 // determine current scalefactor band
		// ^^ there is a kluge above to avoid obtaining scalefactor 21 which is invalid. How is one to deal with the top freqs not covered by scfac bands?
		val exponent: Int = (si.globalGain - 210) - (if (si.scalefacScale == 1) 4 else 2) *
			(scalefactors(sfb) + (if (si.preFlag == 1) ScalefactorBand.preEmphasisFactor(sfb) else 0 ))
		
		Math.signum(is) * Block.fourThirdsPowerTable(Math.abs(is)) * Math.pow(2, 0.25d*exponent) // * Block.twoToThePowerOfXFourths(exponent)
		//Math.signum(is) * Math.pow(Math.abs(is), 4.0/3.0) * Math.pow(2, 0.25d*exponent)
	}
		
	//returns the length in bits for the scalefactor number n (it's variable)
	//based on the other parameters contained in the granule side info.
	def scalefactorLength(n: Int): Option[Int] = { //assumes long windows
	  //if this band is NOT being transmitted for this granule, effectively it means the length is 0
	  if (!ScalefactorBand.isTransmitted(n, scfsi)) Some(0)
	  //otherwise proceed as per specification.
	  else if (n >=  0 && n <= 10) Some(si.slen1)
	  else if (n >= 11 && n <= 20) Some(si.slen2)
	  else None
	}
	    
	//returns the offset in bits (with respect to the beginning of the scalefactors)
	//at which the scalefactor number n begins
	def scalefactorOffset(n: Int): Option[Int] = {
	  if (si.blockType == SHORT_WINDOWS) {
	    if (si.bsSwitchPoint == 1) throw new Exception("Switched blocks not supported yet!")
	    else if (n < 0 || n > MAX_SCFB_SHORT_WINDOWS) None
	    else if (n == 0) Some(0)
		else Some(scalefactorLength(n-1).get + scalefactorOffset(n-1).get)
	  } else {
	    if (n < 0 || n > MAX_SCFB_LONG_WINDOWS) None
		else if (n == 0) Some(0)
	    else Some(scalefactorLength(n-1).get + scalefactorOffset(n-1).get)
	  }
	}
}

object Block {
  private lazy val twoToThePowerOfPlusXFourthsTable: Array[Double] = Array.tabulate(256)(x => Math.pow(2, 0.25d * x))
  private lazy val twoToThePowerOfMinusXFourthsTable: Array[Double] = Array.tabulate(256)(x => Math.pow(2, -0.25d * x))

  val fourThirdsPowerTable: Array[Double] = Array.tabulate(8208)(Math.pow(_, 4.0/3.0))
  
  def twoToThePowerOfXFourths(x: Int): Double = {
    if (x > 0) twoToThePowerOfPlusXFourthsTable(x)
    else if (x == 0) 1
    else if (x < 0) twoToThePowerOfMinusXFourthsTable(-x)
    0 //this line should never be reached
  }
}